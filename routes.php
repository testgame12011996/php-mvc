<?php
	/* Add routes:
	 * With method: new Routes('Controller', 'Action', 'Method');
	 * With all function with method GET: Controller name;
	 */
	$routes = array(
		'root' => new Routes('UsersController', 'login'),
		'errors' => new Routes('ErrorsController'),
		'login' => new Routes('UsersController', ['GET' => 'login', 'POST' => 'login']),
		'my' => array(
			'books' => 'UsersController',
			'index' => new Routes('BooksController', 'index', 'POST'),
			'oke' => new Routes('BooksController', 'index', 'GET'),
			'reset-password' => new Routes('UsersController', 'show', 'GET')
		)
	);

?>