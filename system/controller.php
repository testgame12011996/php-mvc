<?php
	/**
	 * 
	 */
	class Controller
	{
		public $render_format='html';
		public $file_render = 'index';
		public $exception = array();
		private $action = null;
		
		function __construct(){

			$headers = getallheaders();
			if(isset($headers['Content-Type'])){
				if($headers['Content-Type']=='application/json'){
					$this->render_format = 'json';
				}
			}

			if(isset($_GET['format'])){
				if($_GET['format']=='json') $this->render_format = 'json';
			}

			$this->init();
		}
		function init(){}
		
		function view(){
			if(!is_null($this->action)){
				$_GET['action'] = $this->action;
			}
			if(isset($_GET['action'])){
				if(method_exists(get_class($this),$_GET['action'])){
					$this->run($_GET['action']);
					return;
				}
			} else{
				if(method_exists(get_class($this),'index')){
					$this->run('index');
					return;
				}
			}
		}
		function run($function){
			$this->$function();
		}

		function render(){
			global $controller;
			if($this->render_format=='json'){
				exit($this->view());
			}
			if($this->render_format=='html'){
				exit(require_once render($this->file_render));
			}
		}
		function exception(){
			if(!isset($this->exception) && is_null($this->exception)) return false;

			if(is_array($this->exception)){
				$controller_name = get_class($this);
				if(isset($this->exception[$controller_name])&&array_key_exists($controller_name, $this->exception)){
					if($this->exception[$controller_name] == 'all'){
						return true;
					}
					if(isset($_GET['action'])){
						if(is_array($this->exception[$controller_name])){
							if($this->exception[$controller_name] == $_GET['action']) return true;
						} else{
							if($this->exception[$controller_name] == $_GET['action']) return true;
						}
					}
					return false;
				}
				return false;
			}
			return false;
		}
		function setAction($action){
			$this->action = $action;
		}
	}


?>