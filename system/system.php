<?php
	error_reporting(E_ALL);
	if(!session_id()) session_start();

	/* Class routes */
	class Routes{
		public $controller, $method_actions;
		function __construct($controller, $action='index', $method='GET'){
			$this->controller = $controller;
			$this->method_actions = [
				'GET' => NULL,
				'POST' => NULL,
				'PATCH' => NULL,
				'PUT' => NULL,
				'DELETE' => NULL
			];
			if( is_array($action) ){
				$this->method_actions = array_merge($this->method_actions, $action);
			} else { 
				$this->method_actions[$method] = $action;
			}
		}
	}

	require_once 'functions.php';
	require_once 'config.php';
	require_once 'db.php';
	require_once 'model.php';
	require_once 'controller.php';

	/* Autoload class in model, controller folder */
	function auto_load($file_name){
		$split = explode('controller', strtolower($file_name));
		if( count($split) > 1 ) $name = 'controller/'.$split[0].'_controller.php';
		else $name = 'model/'.$split[0].'.php' ;
		return include_once($name);
	}

	spl_autoload_register('auto_load');

	function link_controller($link, $list_link, $action = null, $link_ch = null){
		if( array_key_exists($link, $list_link) ){
			if ( is_array($list_link[$link]) ){
				return link_controller($link_ch, $list_link[$link], $action, null);
			} elseif( is_object($list_link[$link]) && get_class($list_link[$link]) == 'Routes'){
				$rout = $list_link[$link];
				$controller = new $rout->controller;
				$method_actions = $rout->method_actions[$_SERVER['REQUEST_METHOD']];
				if( is_null($action) ) $action = $method_actions;
			} elseif ( is_string($list_link[$link]) ) {
				$controller = new $list_link[$link];
				if(is_null($action)) $action = 'index';
			}
			if ( $link == 'errors' ) $action = 'index';
			if( method_exists($controller, $action) ){
				$controller->setAction($action);
				return $controller;			
			}
		}
		global $routes;
		// return link_controller('errors', $routes, 'index');
	}

	/* Throw error if missing routes.php file */
	if(!file_exists('routes.php')){
		throw new Exception("Missing routes.php file. Please, create routes.php file");
	} else{
		require_once 'routes.php';
	}
	/* Read routes */
	if ( !isset($routes) || !is_array($routes) ){
		throw new Exception("No define routes. Set routes in path 'routes.php'");
	} elseif ( !array_key_exists('root', $routes) || !array_key_exists('errors', $routes) ){
		throw new Exception('$routes need to have element "root" and "errors"');
	}
	else{
		if(!isset($_GET['link'])) $_GET['link'] = 'root';
		if(!isset($_GET['link_ch'])) $_GET['link_ch'] = null;
		if(!isset($_GET['action'])) $_GET['action'] = null;

		$controller = link_controller($_GET['link'], $routes, $_GET['action'], $_GET['link_ch']);
	}

	$controller->render();

?>