<?php

	$ab_url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

	$rootDir= strlen(dirname($_SERVER['SCRIPT_NAME']))>1 ? dirname($_SERVER['SCRIPT_NAME']).'/' : "/" ;

	function render($file_name_or_data,$format=null){
		if($format=='json'){
			exit (json_encode($file_name_or_data, JSON_UNESCAPED_UNICODE));
		}
		$file_name = strtolower('view/'.$file_name_or_data.'.php');
		if(!file_exists($file_name))
			throw new Exception("Can't open file ".$file_name);
		return $file_name;
	}
	function redirect($url){
		header("Location: ".$url);
	}
	function redirect_path($path,$json=null){
		header("Location: ".$GLOBALS['rootDir'].$path);
	}
	function addCSS(array $arr){
		foreach ($arr as $key => $value) {
			if(!strrpos($value, '//')) $value = $GLOBALS['rootDir'].$value;
			echo '<link rel="stylesheet" type="text/css" href="'.$value.'" >';
		}
	}
	function addSCSS(array $arr){
		foreach ($arr as $key => $value) {
			if(!strrpos($value, '//')) $value = $GLOBALS['rootDir'].$value;
			echo '<link rel="stylesheet" type="text/scss" href="'.$value.'">';
		}
	}
	function addJS(array $arr){
		foreach ($arr as $key => $value) {
			if(!strrpos($value, '//')) $value = $GLOBALS['rootDir'].$value;
			echo '<script src="'.$value.'"></script>';
		}
	}
	function link_to($url,$class="",$option=""){
		if(!strrpos($url, '//')) $url = $GLOBALS['rootDir'].$url;
		echo '<a href="'.$url.'" class="btn btn-danger">Delete</a>';
	}
	function map(array $input,array $listkey){
		$GLOBALS['listkey'] = $listkey;
		return array_map(function($v){
			global $listkey;
			$arr = [];
			foreach ($listkey as $key => $value) {
				if(isset($v[$value])&&array_key_exists($value,$v)){
					$arr[$value] = $v[$value];
				}
			}
			return $arr;
		},$input);
	}

?>