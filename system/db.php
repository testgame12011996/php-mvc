<?php
	/**
	 * 
	 */
	class DB
	{
		public $db;	
		public $table_name;
		
		private function connect(){
			global $config;
			$this->db = new mysqli($config['database']['host'], $config['database']['username'],$config['database']['password'],$config['database']['db_name']);
			$this->db->set_charset("utf8");
		}
		private function disconnect(){
			$this->db->close();
		}
		/**
		*Ex: $db->generate(['name varchar(255) NOT NULL', 'email varchar(255) UNIQUE'])
		*/
		function generate(array $columns){
			$str_query = 'CREATE TABLE IF NOT EXISTS '.$this->table_name.' (id Integer PRIMARY KEY AUTO_INCREMENT,created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,'.strtolower(implode(',', $columns)).')';
			$this->connect();
			$query = $this->db->query(strtolower($str_query));
			$this->disconnect();
			if(!$query){
				throw new Exception('Generate table is fail: '.strtolower($str_query)); 
			}
		}
		/**
		*Ex: $db->addColumn('email', 'varchar(255)', 'UNIQUE');
		*/		
		function addColumn($name, $type, $option=''){
			$str_query = 'ALTER TABLE '.$this->table_name.' ADD '.$name.' '.$type.' '.$option;
			$this->connect();
			$query = $this->db->query(strtolower($str_query));
			$this->disconnect();
			return $query;
		}
		/**
		*Ex: $db->addUnique('email');
		*/	
		function addUnique($col_name){
			$str_query = 'ALTER TABLE '.$this->table_name.' ADD UNIQUE ('.$col_name.');';
			$this->connect();
			$query = $this->db->query(strtolower($str_query));
			$this->disconnect();
			return $query;
		}
		/**
		*Ex: $db->create(['name'=>'Lady Gara', 'email'=>'test@example.com']);
		*/	
		function create(array $values){
			$cols = implode(',',array_keys($values));
			$vals = implode('","', $values);
			$str_query = 'INSERT INTO '.$this->table_name.'('.strtolower($cols).') VALUES("'.$vals.'")';
			$this->connect();
			$query = $this->db->query($str_query);
			$this->disconnect();
			return $query;
		}
		/**
		*Ex: $db->update(['name'=>'Lady Gara', 'email'=>'test@example.com'], ['col'=>'id', 'value'=>'1', 'type'=>'=']);
		*Ex: $db->update(['name'=>'Lady Gara', 'email'=>'test@example.com'], ['col'=>'id', 'value'=>['1','2'], 'type'=>'IN']);
		*/	
		function update(array $values,array $conditions=null){
			$str_condition = null;
			if($conditions!=null)
				$str_condition = ' WHERE '.$this->convertCondition($conditions);
			$values = implode(', ', array_map(
				    function ($v, $k) {
				        if(!is_array($v)){
				            return strtolower($k).'="'.$v.'"';
				        }
				    }, 
				    $values, 
				    array_keys($values)
				));
			$str_query = 'UPDATE '.$this->table_name.' SET '.$values.$str_condition;
			$this->connect();
			$query = $this->db->query($str_query);
			$this->disconnect();
			return $query;
		}
		/**
		*Ex: $db->delete(['col'=>'id', 'value'=>'1'],['col'=>'email', 'value'=>'test@example.com']);
		*/	
		function delete(){
			$str_condition = '';
			$conditions = func_get_args();
			$str_condition = $this->convertCondition($conditions);
			$str_query = 'DELETE FROM '.$this->table_name.' WHERE '.$str_condition;
			$this->connect();
			$query = $this->db->query($str_query);
			$this->disconnect();
			return $query;
		}
		/**
		*Ex: $db->get_all('created_at desc');
		*/	
		function get_all($order="id desc"){
			$str_query = 'SELECT * FROM '.$this->table_name.' ORDER BY '.$order;
			$this->connect();
			$query = $this->db->query($str_query);
			$this->disconnect();
			return $this->fetch_data($query);
		}
		/**
		*Ex: $db->get_by_id(1);
		*/	
		function get_by_id($id, $order="id desc"){
			$str_query = 'SELECT * FROM '.$this->table_name.' WHERE id="'.$id.'" ORDER BY '.$order;
			$this->connect();
			$query = $this->db->query($str_query);
			$this->disconnect();
			return $this->fetch_data($query)[0];
		}
		/**
		*Ex: $db->get_by_ids([1,2,3]);
		*/	
		function get_by_ids(array $ids, $order="id desc"){
			$ids =  '('.implode(',', $ids).')';	
			$str_query = 'SELECT * FROM '.$this->table_name.' WHERE id IN '.$ids.' ORDER BY '.$order;
			$this->connect();
			$query = $this->db->query($str_query);
			$this->disconnect();
			return $this->fetch_data($query);
		}
		/**
		*Ex: $db->get_by('email', 'test@example.com');
		*/	
		function get_by($column_name, $condition, $order="id desc"){
			$str_query = 'SELECT * FROM '.$this->table_name.' WHERE '.strtolower($column_name).' like "%'.$condition.'%" ORDER BY '.$order;
			$this->connect();
			$query = $this->db->query($str_query);
			$this->disconnect();
			return $this->fetch_data($query);
		}
		/**
		*Ex: $db->search(['col'=>'email', 'value'=>'test@example.com'], ['col'=>'password', 'value'=>'123456789']);
		*/	
		function search(){
			$str_condition = '';
			$conditions = func_get_args();
			$str_condition = $this->convertCondition($conditions);
			$str_query = 'SELECT * FROM '.$this->table_name.' WHERE '.$str_condition;
			$this->connect();
			$query = $this->db->query($str_query);
			$this->disconnect();
			return $this->fetch_data($query);
		}
		function multi(array $arr){
			$str_query = implode(';', $arr);
			$this->connect();
			$query = $this->db->multi_query($str_query);
			return $query;
		}
		
		function fetch_data($query){
			return $query->fetch_all(MYSQLI_ASSOC);
		}
		function convertCondition(array $conditions){
			$str_condition = '';
			foreach ($conditions as $key => $value) {
				if(!array_key_exists('col', $value)&&!array_key_exists('value', $value)) continue;
				if(!isset($value['type'])) $value['type'] = '=';
				$str_condition .= strtolower($value['col']).' '.$value['type'].'"'.$value['value'].'"';
				if($key<count($conditions)-1) $str_condition.=' and '; 
			}
			return $str_condition;
		}
	}

?>