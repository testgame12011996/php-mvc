<?php
	if(!session_id()) session_start();
	/**
	 * 
	 */
	define('IS_DATA', 'is_data');
	define('IS_BOOL', 'is_bool');

	class ModelSample
	{
		private $db;
		private $table_name;
		public $custom_query, $mysqli_result;
		public $select_params, $where_params, $insert_params, $order_by, $group;

		function __construct()
		{
			$this->table_name = get_class($this).'s';
			$this->order_by = 'id desc';
			$this->init();
		}

		private function connect(){
			global $config;
			$this->db = new mysqli($config['database']['host'], $config['database']['username'],$config['database']['password'],$config['database']['db_name']);
			$this->db->set_charset("utf8");
		}

		private function disconnect(){
			$this->db->close();
		}

		function init(){}

		function initCols(){}

		/* Use to create table */
	 	function generateCols(){
	 		$this->initCols();
	 		$cols = get_object_vars($this);
	 		$assoc = array_filter($cols, function($v, $k){
	 			return gettype($v) == 'object' && get_parent_class($v) == 'ModelSample' ;
	 		}, ARRAY_FILTER_USE_BOTH);
	 		$cols = array_filter($cols, function($v, $k){
	 			return gettype($v) == 'object' && get_class($v) == 'Column';
	 		}, ARRAY_FILTER_USE_BOTH);
	 		foreach ($assoc as $key => $value) {
	 			$cols[$key.'_id'] = new Column($key.'_id', 'int');
	 		}
	 		return array_values(array_map(function($col){
	 			return $col->generate();
	 		}, $cols));
	 	}	

	 	/* get cols */
	 	function cols(){
	 		$this->initCols();
	 		$cols = get_object_vars($this);
	 		$assoc = array_filter($cols, function($v, $k){
	 			return gettype($v) == 'object' && get_parent_class($v) == 'ModelSample' ;
	 		}, ARRAY_FILTER_USE_BOTH);
	 		$cols = array_filter($cols, function($v, $k){
	 			return gettype($v) == 'object' && get_class($v) == 'Column';
	 		}, ARRAY_FILTER_USE_BOTH);
	 		foreach ($assoc as $key => $value) {
	 			$cols[$key.'_id'] = new Column($key.'_id', 'int');
	 		}
	 		return array_map(function($val){
	 			return '';
	 		}, $cols);
	 	}

	 	/* Custom query string */
	 	function custom(string $value){
	 		$this->custom_query = $value; 
	 		return $this;
	 	}

		/* add select query params*/
		function select($val = '*'){
			if(is_null($this->select_params)) $this->select_params = [];

			if(!is_null($val) && is_array($val)){
				$this->select_params = array_merge($this->select_params, $val);
				$this->insert_params = null;
				return $this;
			} elseif (!is_null($val) && is_string($val)) {
				if($val == '*') $this->select_params = [];
				$this->insert_params = null;
				array_push($this->select_params, $val);
				return $this;
			}
			return false;
		}

		/* 
		* Add insert query params
		* It's anonymous array
		*/
		function insert(array $val){
			$this->insert_params = $this->check_data_params($val);
			if(count($this->insert_params) > 0) $this->select_params = null;
			return $this;
		}

		/* add condition query params(where) */
		function where($val){
			if(is_null($this->where_params)) $this->where_params = [];

			if(!is_null($val) && is_array($val)){
				array_merge($this->where_params, $val);
				return $this;
			} elseif (!is_null($val) && is_string($val)) {
				array_push($this->where_params, $val);
				return $this;
			}
			return false;
		}

		/* set columns order */
		function order(string $val){
			$this->order_by = $val;
			return $this;
		}
		/* Group by */
		function group(string $val){
			$this->group = $val;
			return $this;
		}

		/* Excute query */
		function exec($result_type = IS_BOOL){
			$result = false;
			$this->connect();

			if(!is_null($this->custom_query)){
				$this->custom_query;
			}
			if(!is_null($this->insert_params) && is_array($this->insert_params) && count($this->insert_params)){
				$cols = implode(',', array_keys($this->insert_params));
				$vals = implode('","', $this->insert_params);

				$str_query = 'INSERT INTO '.$this->table_name.'('.strtolower($cols).') VALUES("'.$vals.'")';

				$result = $this->db->query($str_query);
			}
			if(!is_null($this->select_params) && is_array($this->select_params) && count($this->select_params)){
				$cols = implode(',', $this->select_params);
				if(!is_null($this->where_params) && is_array($this->where_params) && count($this->where_params))
					$condition = ' WHERE '.implode(' AND ', $this->where_params);
				else $condition = '';
				$str_query = 'SELECT '.$cols.' FROM '.$this->table_name.$condition;

				if(!is_null($this->group)) {
					$str_query.=' GROUP BY '.$this->group;
				} 
				$str_query.=' ORDER BY '.$this->order_by;
				$this->mysqli_result = $this->db->query($str_query);

				$result = true;
				if($result_type == IS_DATA){
					$result = $this->data();
				}	
			}
			$this->disconnect();
			return $result;
		}

		/* Protect params */
		function check_data_params($val){
			$cols = $this->cols();
			return array_intersect_key($val, $this->cols());
		}

		/* Fetch data*/
		function data(){
			if( gettype($this->mysqli_result) == 'object' && get_class($this->mysqli_result) == 'mysqli_result' ){
				$data = [];

				while($row = $this->mysqli_result->fetch_assoc()){
					array_push($data, $row);
				}

				return $data;
			}
			return null;
		}
	}

	/**
	 * Column class
	 */
	class Column
	{
		public $name, $type, $size;
		public $options;
		function __construct($name, $type, $size=null,array $options=null)
		{
			$this->name = $name;
			$this->type = Type::get($type);
			$this->size = $size;
			$this->options = $options;
		}

		function generate(){
			$res = $this->name .' '. $this->type;
			if(!is_null($this->size) && is_int($this->size)) $res .= '('. $this->size .') ';
			if(!is_null($this->options)) $res .= implode(' ', $this->options);
			return $res;
		}
	}

	/**
	 * 
	 */
	class Type
	{	
		static public function get($str_type){
			switch ($str_type) {
				case 'string':
					return 'varchar';
				case 'int':
					return 'integer';
				case 'timestamp':
					return 'timestamp';
				case 'char':
					return 'char';
				case 'text':
					return 'text';
				case 'bool':
					return 'boolean';
				default:
					return 'varchar';
			}
		}
	}
?>