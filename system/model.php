<?php
	/**
	 * Model class
	 */
	class Model extends DB
	{
	 	function __construct(){
	 		$this->table_name = get_class($this).'s';
	 	}

	 	function initCols(){}

	 	function getColsName(){
	 		$this->initCols();
	 		$cols = get_object_vars($this);
	 		$cols = array_filter($cols, function($v, $k){
	 			return gettype($v) == 'object' && get_class($v) == 'Column';
	 		}, ARRAY_FILTER_USE_BOTH);
	 		return array_values(array_map(function($val){
	 			return $val->generate();
	 		}, $cols));
	 	}	
	}


	/**
	 * Column class
	 */
	class Column
	{
		public $name, $type, $size;
		public $options;
		function __construct($name, $type, $size=null,array $options=null)
		{
			$this->name = $name;
			$this->type = Type::get($type);
			$this->size = $size;
			$this->options = $options;
		}

		function generate(){
			$res = $this->name .' '. $this->type;
			if(!is_null($this->size)) $res .= '('. $this->size .') ';
			if(!is_null($this->options)) $res .= implode(' ', $this->options);
			return $res;
		}
	}

	/**
	 * 
	 */
	class Type
	{	
		static public function get($str_type){
			switch ($str_type) {
				case 'string':
					return 'string';
				case 'int':
					return 'integer';
				case 'timestamp':
					return 'timestamp';
				case 'char':
					return 'char';
				case 'text':
					return 'text';
				default:
					return 'varchar';
			}
		}
	}

?>