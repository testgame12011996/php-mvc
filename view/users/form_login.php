<form method="post">
  <div class="form-group">
    <label>Email address</label>
    <input type="email" class="form-control" placeholder="Enter email" name="email">
    <small >We'll never share your email with anyone else.</small>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" class="form-control"  placeholder="Password" name="password">
  </div>
  <button type="submit" class="btn btn-primary" name="login">Submit</button>
</form>