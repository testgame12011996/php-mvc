<div class="w-100 text-right">
  <a href="books/create" class="btn btn-primary ">New</a>
</div>
<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">First</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
  <?php  
    foreach ($books as $key => $value):
  ?>
    <tr>
      <th scope="row"><?php echo $key+1;?></th>
      <td><a href="<?= 'books/'.$value['id'].'/show';?>"><?= $value['name'];?></a></td>
      <td>
        <a href="<?= 'books/'.$value['id'].'/edit';?>" class="btn btn-info">Edit</a>
        <a href="<?= 'books/'.$value['id'].'/delete';?>" class="btn btn-danger">Delete</a>
      </td>
    </tr>
  <?php endforeach; ?>
  </tbody>
</table>