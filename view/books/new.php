<form method="post">
  <div class="form-group">
    <label>Name</label>
    <input type="text" class="form-control" placeholder="Enter name" name="name" value="<?= isset($book) ? $book['name']:''; ?>">
  </div>
  <button type="submit" class="btn btn-primary" name="<?= isset($book) ? 'update':'create'; ?>"><?= isset($book) ? 'Update':'Create'; ?></button>
</form>