<?php
	
	/**
	* 
	*/
	class MainController extends Controller
	{
		
		function init()
		{
			$this->exception = [
				'UsersController'=>'login'
			];

			// if($this->exception()) return;

			// $this->checkUser();
		}

		function checkUser(){
			global $ab_url;
			if(!isset($_COOKIE['uid'])){
				redirect_path('users/login?redirect='.$ab_url);
			}
		}

		function js(){
			$js = glob('assets/js/*.js');
			addJS($js);
		}
		function style(){
			$css = glob('assets/css/*.css');
			addCSS($css);

			$scss = glob('assets/css/*.scss');
			addSCSS($scss);

		}
	}

?>