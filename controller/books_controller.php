<?php
	/**
	 * 
	 */
	class BooksController extends MainController
	{
		function index()
		{
			$books = (new Book())->get_all();
			
			if($this->render_format=="json"){
				$books = map($books,['id','name']);
				render($books,'json');
			}
			require_once render('books/list');
		}
		function create(){
			if(isset($_POST['create'])){
				if((new Book())->create(['name'=>$_POST['name']])){
					// redirect_path('books');
				}
			}
			require_once render('books/new');
		}
		function delete(){
			$book = new Book();

			if($book->delete(['col'=>'id','value'=>$_GET['id']])){
				exit(redirect_path('books'));
			} else{
				echo 'not oke';
			}
		}
		function edit(){
			if(isset($_GET['id'])){
				$book = new Book();

				if(isset($_POST['update'])){
					if($book->update(
						[
							'name'=>$_POST['name']
						],
						[
							['col'=>'id','value'=>$_GET['id']]
						]
					)){
						exit(redirect_path('books'));
					}
				}

				$book = $book->get_by_id($_GET['id']);
				require_once render('books/new');
			}
		}


		function show(){
			if(isset($_GET['id'])){
				$book = (new Book())->get_by_id($_GET['id']);
				require_once render('books/show');
			}
		}
	}
?>