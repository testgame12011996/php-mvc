<?php
	/**
	 * 
	 */
	class UsersController extends MainController
	{

		function login()
		{
			if(isset($_COOKIE['uid'])){
				redirect_path('users/show');
			}

			if(isset($_POST['login'])){
				
				$user = (new User())->search(
					['col'=>'email','value'=>$_POST['email']],
					['col'=>'password','value'=>md5($_POST['password'])]
				);

				if(count($user)>0) {

					setcookie('uid',$user[0]['id'],time()+3600);

					if(isset($_GET['redirect'])){
						exit(redirect($_GET['redirect']));
					}
					
					redirect_path('users/show');
				}
				else require_once render('users/form_login');
			}
			else
				require_once render('users/form_login');
		}

		function show(){
			$book = (new User())->get_by_id($_COOKIE['uid']);
			require_once render('books/show');
		}

		function logout(){
			unset($_SESSION['user']);

			if(isset($_GET['redirect'])){
				redirect($_GET['redirect']);
			}

			redirect_path('users/login');
		}
		
		function style(){
			parent::style();
		}
	}
?>